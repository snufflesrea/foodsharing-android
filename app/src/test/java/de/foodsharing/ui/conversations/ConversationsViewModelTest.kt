package de.foodsharing.ui.conversations

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.R
import de.foodsharing.api.UserResponse
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.services.AuthService
import de.foodsharing.services.ConversationsService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomConversations
import de.foodsharing.test.createRandomUser
import de.foodsharing.test.createRandomUserResponse
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.IOException

class ConversationsViewModelTest {
    @Mock
    lateinit var conversationsService: ConversationsService
    @Mock
    lateinit var authService: AuthService

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun fetch() {
        val currentUserResponse = createRandomUserResponse()
        val currentUser = createRandomUser(currentUserResponse.id)
        val conversations = createRandomConversations(20, currentUser)
        val conversationsSecondPage = createRandomConversations(20, currentUser)

        val authSubject = PublishSubject.create<UserResponse>()
        whenever(authService.currentUser()) doReturn authSubject

        val conversationsSubject = PublishSubject.create<List<ConversationListEntry>>()
        var reloadEvent: Observable<Any>? = null
        var loadNextEvent: Observable<Any>? = null
        var errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)? = null
        whenever(conversationsService.listPaged(any(), any(), any())) doAnswer {
            reloadEvent = it.getArgument(0)
            loadNextEvent = it.getArgument(1)
            errorHandler = it.getArgument(2)
            conversationsSubject
        }

        val viewModel = ConversationsViewModel(conversationsService, authService)
        viewModel.conversationsWithCurrentUser.observeForever {}
        viewModel.isLoading.observeForever {}

        viewModel.assert(
            isLoading = true
        )

        authSubject.onNext(currentUserResponse)

        viewModel.assert(
            isLoading = true,
            userResponse = currentUserResponse
        )

        conversationsSubject.onNext(conversations)

        viewModel.assert(
            conversations = conversations,
            userResponse = currentUserResponse
        )

        /*
         * Test reloading the conversations
         */

        var reloadCalled = false
        reloadEvent?.subscribe {
            reloadCalled = true
        }
        viewModel.refresh()
        Assert.assertEquals(true, reloadCalled)

        viewModel.assert(
            isReloading = true,
            conversations = conversations,
            userResponse = currentUserResponse
        )

        conversationsSubject.onNext(conversations)

        viewModel.assert(
            conversations = conversations,
            userResponse = currentUserResponse
        )

        /*
         * Test loading the next page
         */

        var nextPageCalled = false
        loadNextEvent?.subscribe {
            nextPageCalled = true
        }
        viewModel.loadNext()
        Assert.assertEquals(true, nextPageCalled)

        viewModel.assert(
            isLoading = true,
            conversations = conversations,
            userResponse = currentUserResponse
        )

        conversationsSubject.onNext(conversations + conversationsSecondPage)

        viewModel.assert(
            conversations = conversations + conversationsSecondPage,
            userResponse = currentUserResponse
        )
    }



    @Test
    fun fetchWithErrors() {
        val currentUserResponse = createRandomUserResponse()
        val currentUser = createRandomUser(currentUserResponse.id)
        val conversations = createRandomConversations(20, currentUser)
        val conversationsSecondPage = createRandomConversations(20, currentUser)

        var authSubject = PublishSubject.create<UserResponse>()
        whenever(authService.currentUser()) doReturn Observable.create { o ->
            authSubject = PublishSubject.create<UserResponse>()
            authSubject.subscribe({
                o.onNext(it)
            }, {
                o.onError(it)
            }, {
                o.onComplete()
            })
        }

        val conversationsSubject = PublishSubject.create<List<ConversationListEntry>>()
        var reloadEvent: Observable<Any>? = null
        var loadNextEvent: Observable<Any>? = null
        var errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)? = null
        whenever(conversationsService.listPaged(any(), any(), any())) doAnswer {
            reloadEvent = it.getArgument(0)
            loadNextEvent = it.getArgument(1)
            errorHandler = it.getArgument(2)
            conversationsSubject
        }

        val viewModel = ConversationsViewModel(conversationsService, authService)
        viewModel.conversationsWithCurrentUser.observeForever {}
        viewModel.isLoading.observeForever {}

        val conversationsErrorSubject = PublishSubject.create<Throwable>()
        errorHandler?.invoke(conversationsErrorSubject)?.subscribe(object : Observer<Any> {
            override fun onComplete() {}
            override fun onSubscribe(d: Disposable) {}
            override fun onNext(t: Any) {}
            override fun onError(e: Throwable) {}
        })

        viewModel.assert(
            isLoading = true
        )

        conversationsErrorSubject.onNext(IOException())

        viewModel.assert(
            errorState = R.string.error_no_connection
        )

        viewModel.tryAgain()

        viewModel.assert(
            isLoading = true
        )

        conversationsSubject.onNext(conversations)

        viewModel.assert(
            isLoading = true,
            conversations = conversations
        )

        authSubject.onError(IOException())

        viewModel.assert(
            errorState = R.string.error_no_connection,
            conversations = conversations
        )

        viewModel.tryAgain()
        conversationsSubject.onNext(conversations)

        viewModel.assert(
            isLoading = true,
            conversations = conversations
        )

        authSubject.onNext(currentUserResponse)

        viewModel.assert(
            isLoading = false,
            userResponse = currentUserResponse,
            conversations = conversations
        )

        var nextPageCalled = false
        loadNextEvent?.subscribe {
            nextPageCalled = true
        }
        viewModel.loadNext()
        Assert.assertEquals(true, nextPageCalled)

        viewModel.assert(
            isLoading = true,
            userResponse = currentUserResponse,
            conversations = conversations
        )

        conversationsErrorSubject.onNext(IOException())

        viewModel.assert(
            errorState = R.string.error_no_connection,
            userResponse = currentUserResponse,
            conversations = conversations
        )

        viewModel.tryAgain()
        authSubject.onNext(currentUserResponse)

        viewModel.assert(
            isLoading = true,
            userResponse = currentUserResponse,
            conversations = conversations
        )

        conversationsSubject.onNext(conversations + conversationsSecondPage)

        viewModel.assert(
            userResponse = currentUserResponse,
            conversations = conversations + conversationsSecondPage
        )
    }

    fun ConversationsViewModel.assert(
        isLoading: Boolean? = false,
        isReloading: Boolean? = false,
        conversations: List<ConversationListEntry>? = null,
        userResponse: UserResponse? = null,
        errorState: Int? = null
    ) {
        Assert.assertEquals(isLoading, this.isLoading.value)
        Assert.assertEquals(isReloading, this.isReloading.value)
        Assert.assertEquals(conversations, this.conversations.value)
        Assert.assertEquals(userResponse, this.currentUser.value)
        val conversationsWithUsers = if (conversations != null || userResponse != null) {
            Pair(conversations, userResponse)
        } else null
        Assert.assertEquals(conversationsWithUsers, this.conversationsWithCurrentUser.value)
        Assert.assertEquals(errorState, this.errorState.value)
    }
}
