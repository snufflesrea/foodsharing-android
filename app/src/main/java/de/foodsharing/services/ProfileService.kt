package de.foodsharing.services

import de.foodsharing.api.Profile
import de.foodsharing.api.ProfileAPI
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ProfileService @Inject constructor(
    private val profileAPI: ProfileAPI
) {

    fun current(): Observable<Profile> {
        return Observable.concat(
                profileAPI.current("only-if-cached").onErrorResumeNext(Observable.empty()),
                profileAPI.current("no-cache"))
                .distinctUntilChanged()
                .map {
                    if (it.lat == "0" && it.lon == "0") {
                        // Coordinate is not set in profile, make this explicit
                        it.copy(lat = null, lon = null)
                    } else {
                        it
                    }
                }
    }
}
