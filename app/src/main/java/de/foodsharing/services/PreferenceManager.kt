package de.foodsharing.services

import android.content.SharedPreferences
import de.foodsharing.utils.SHARED_PREFERENCE_LOGGED_IN
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferenceManager @Inject constructor(
    private val preferences: SharedPreferences
) {
    var isLoggedIn: Boolean
        get() = preferences.getBoolean(SHARED_PREFERENCE_LOGGED_IN, false)
        set(value) = preferences.edit().putBoolean(SHARED_PREFERENCE_LOGGED_IN, value).apply()
}
