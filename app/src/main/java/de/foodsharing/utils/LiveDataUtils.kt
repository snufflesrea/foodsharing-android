package de.foodsharing.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * Creates a new LiveData which combines this instance with the given [liveData]. The aggregation
 * of the data from both sources is defined by the given [block] function.
 *
 * @param liveData the other LiveData object which should be combined with the current instance
 * @param block a function defining the aggregation of both sources
 * @return A new LiveData instance combining the data of both inputs
 */
fun <T, K, R> LiveData<T>.combineWith(
    liveData: LiveData<K>,
    block: (T?, K?) -> R?
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) {
        result.value = block.invoke(this.value, liveData.value)
    }
    result.addSource(liveData) {
        result.value = block.invoke(this.value, liveData.value)
    }
    return result
}