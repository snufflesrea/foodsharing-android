package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity
import de.foodsharing.ui.fsp.FairSharePointActivity
import de.foodsharing.ui.initial.InitialActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.ui.newbasket.NewBasketActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.users.UserListActivity

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun bindInitialActivity(): InitialActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindConversationActivity(): ConversationActivity

    @ContributesAndroidInjector
    abstract fun bindBasketActivity(): BasketActivity

    @ContributesAndroidInjector
    abstract fun bindNewBasketActivity(): NewBasketActivity

    @ContributesAndroidInjector
    abstract fun bindProfileActivity(): ProfileActivity

    @ContributesAndroidInjector
    abstract fun bindUserListActivity(): UserListActivity

    @ContributesAndroidInjector
    abstract fun bindEditBasketActivity(): EditBasketActivity

    @ContributesAndroidInjector
    abstract fun bindFairSharePointActivity(): FairSharePointActivity
}