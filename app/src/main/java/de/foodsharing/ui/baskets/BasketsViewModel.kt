package de.foodsharing.ui.baskets

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.Profile
import de.foodsharing.services.BasketService
import de.foodsharing.services.ProfileService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class BasketsViewModel @Inject constructor(
    private val basketService: BasketService,
    private val profileService: ProfileService
) : BaseViewModel() {

    companion object {
        const val DEFAULT_BASKET_DISTANCE_KM = 30
    }

    val baskets = MutableLiveData<Pair<List<BasketItemModel>, List<BasketItemModel>>>()
    val profile = MutableLiveData<Profile>()

    val isLoading = MutableLiveData<Boolean>().apply {
        value = true
    }
    val showError = MutableLiveData<Event<Int>>()

    val distance = MutableLiveData<Int>().apply {
        value = DEFAULT_BASKET_DISTANCE_KM
    }

    private val refreshEvents = BehaviorSubject.createDefault<Any>(true)

    init {
        // Get coordinate
        val coordinateObservable = profileService.current().switchMap { profile ->
            this.profile.postValue(profile)
            profile.getCoordinates()?.let {
                Observable.just(it)
            } ?: Observable.empty()
        }

        val nearbyObservable = coordinateObservable.switchMap { refCoordinate ->
            val basketsDistance = distance.value ?: DEFAULT_BASKET_DISTANCE_KM
            basketService.listClose(refCoordinate.lat, refCoordinate.lon, basketsDistance)
                .map { basketResponse ->
                    basketResponse.baskets?.map { b ->
                        BasketItemModel(b, b.toCoordinate().distanceTo(refCoordinate))
                    }?.sortedBy {
                        it.distance
                    } ?: emptyList()
                }
        }.defaultIfEmpty(emptyList())

        val myObservable = basketService.list().map {
            it.baskets?.sortedByDescending {
                it.createdAt
            }?.map {
                BasketItemModel(it)
            } ?: emptyList()
        }

        request(refreshEvents.doOnNext {
            isLoading.postValue(true)
        }.switchMap {
            Observables.combineLatest(myObservable, nearbyObservable) { a, b ->
                a to b
            }.subscribeOn(Schedulers.io())
        }, {
            isLoading.value = false
            baskets.value = it
        }, {
            isLoading.value = false
            showError.value = Event(R.string.error_unknown)
        })
    }

    fun reload() {
        refreshEvents.onNext(true)
    }
}