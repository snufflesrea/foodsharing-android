package de.foodsharing.ui.baskets

import de.foodsharing.model.Basket

data class BasketItemModel(
    val basket: Basket,
    val distance: Double? = null
)