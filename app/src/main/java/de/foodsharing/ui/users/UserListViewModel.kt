package de.foodsharing.ui.users

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.utils.CachedResourceLoader
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import de.foodsharing.utils.Utils
import javax.inject.Inject

class UserListViewModel @Inject constructor(
        private val resourceLoader: CachedResourceLoader
) : BaseViewModel() {

    val userList = MutableLiveData<List<User>>()
    val pictures = MutableLiveData<MutableList<Bitmap?>>()

    var throwableMessage: String = ""

    fun loadUserPicture(user: User): Bitmap? {
        var picture: Bitmap? = null
        request(resourceLoader.createImageObservable(Utils.getUserPhotoURL(user, Utils.PhotoType.Q_130), DEFAULT_USER_PICTURE),
                { picture = it }, { throwableMessage = it.toString() })
        return picture
    }
}