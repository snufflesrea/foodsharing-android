package de.foodsharing.ui.users

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.model.User
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_user.view.item_name
import kotlinx.android.synthetic.main.item_user.view.item_picture

class UserListAdapter(
    private val users: List<User>,
    private val pictures: List<Bitmap?>,
    private val listener: UserListActionListener,
    val context: Context
) : RecyclerView.Adapter<UserListAdapter.UserHolder>() {

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(users[position], pictures[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): UserHolder {
        return UserHolder(parent.inflate(R.layout.item_user, false), listener, context)
    }

    class UserHolder(
        val view: View,
        private val listener: UserListActionListener,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var user: User? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            user?.let {
                listener.onViewUser(it)
            }
        }

        fun bind(user: User, picture: Bitmap?) {
            this.user = user

            view.item_name.text = user.getDisplayName(context)
            picture?.let { view.item_picture.setImageBitmap(it) }
        }
    }
}