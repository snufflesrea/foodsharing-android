package de.foodsharing.ui.users

import de.foodsharing.model.User
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.CachedResourceLoader
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import de.foodsharing.utils.Utils
import javax.inject.Inject

class UserListPresenter @Inject constructor(
    private val resourceLoader: CachedResourceLoader
) :
    BasePresenter<UserListContract.View>(), UserListContract.Presenter {

    override fun loadUserPicture(user: User) {
        request(
            resourceLoader.createImageObservable(
                Utils.getUserPhotoURL(user, Utils.PhotoType.Q_130), DEFAULT_USER_PICTURE
            ), {
            it.setHasAlpha(true)
            view?.displayUserPicture(user, it)
        }, {
            view?.showErrorMessage(it)
        })
    }
}
