package de.foodsharing.ui.users

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_user_list.*
import javax.inject.Inject

class UserListActivity : BaseActivity(), UserListActionListener, Injectable {

    companion object {
        const val EXTRA_USERS = "users"
        const val EXTRA_TITLE = "title"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val userListViewModel: UserListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(UserListViewModel::class.java)
    }

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: UserListAdapter
    private lateinit var users: List<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.user_list_content
        setContentView(R.layout.activity_user_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title =
            if (intent.hasExtra(EXTRA_TITLE)) intent.getStringExtra(EXTRA_TITLE)
            else ""

        if (intent.hasExtra(EXTRA_USERS)) {
            users = intent.getSerializableExtra(EXTRA_USERS) as ArrayList<User>
            userListViewModel.userList.value = users
        } else {
            userListViewModel.userList.value = emptyList()
        }
        userListViewModel.pictures.value = MutableList(userListViewModel.userList.value!!.size) { null }

        layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager
        adapter = UserListAdapter(userListViewModel.userList.value!!, userListViewModel.pictures.value!!, this, this)
        recycler_view.adapter = adapter

        userListViewModel.userList.value?.forEach {
            val user = it
            val getpicture = userListViewModel.loadUserPicture(it)
            getpicture?.let { displayUserPicture(user, getpicture) }
                    ?: showErrorMessage(userListViewModel.throwableMessage)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    fun showErrorMessage(error: String) {
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    fun displayUserPicture(user: User, picture: Bitmap) {
        val idx = userListViewModel.userList.value!!.indexOf(user)
        if (idx in 0..userListViewModel.userList.value!!.size) {
            userListViewModel.pictures.value!![idx] = picture
            adapter.notifyDataSetChanged()
        }
    }

    override fun onViewUser(user: User) {
        startActivity(Intent(this, ProfileActivity::class.java).apply {
            putExtra(ProfileActivity.EXTRA_USER, user)
        })
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
