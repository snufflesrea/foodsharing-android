package de.foodsharing.ui.base

import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.utils.Utils

abstract class BaseFragment : Fragment() {

    protected fun showMessage(message: String, duration: Int = Snackbar.LENGTH_LONG) {
        activity?.let {
            Utils.showSnackBar(
                it.findViewById(android.R.id.content),
                message,
                ContextCompat.getColor(it, R.color.colorAccent),
                duration
            ).show()
        }
    }
}
