package de.foodsharing.ui.conversation

import android.text.Html
import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.IUser
import de.foodsharing.model.Message
import de.foodsharing.model.User
import java.util.Date

data class ChatkitMessage(
    val message: Message,
    private val user: IUser
) : IMessage {

    constructor(message: Message, author: User) : this(message, ChatkitUser(author))
    constructor(message: Message) :
        this(message, ChatkitUser(User(message.fsId, "", message.fsName, message.fsPhoto, message.fsPhoto)))

    override fun getId() = message.id.toString()

    @Suppress("DEPRECATION")
    override fun getText() = Html.fromHtml(message.body.replace("\n", "<br/>")).toString()

    override fun getUser() = user

    override fun getCreatedAt(): Date = message.time
}