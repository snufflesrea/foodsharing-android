package de.foodsharing.model

import android.os.Parcel
import android.os.Parcelable
import de.foodsharing.utils.OsmdroidUtils.toGeoPoint

/**
 * Interface for any object that has a coordinate.
 */
interface ICoordinate {
    val lat: Double
    val lon: Double

    fun toCoordinate(): Coordinate {
        return Coordinate(lat, lon)
    }
}

/**
 * Object representing a coordinate.
 */
data class Coordinate(
    override val lat: Double,
    override val lon: Double
) : ICoordinate, Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readDouble(),
            parcel.readDouble()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Coordinate> {
        override fun createFromParcel(parcel: Parcel): Coordinate {
            return Coordinate(parcel)
        }

        override fun newArray(size: Int): Array<Coordinate?> {
            return arrayOfNulls(size)
        }
    }

    /**
     * Returns the distance in meters to the given coordinate.
     */
    fun distanceTo(coord: Coordinate): Double {
        return toGeoPoint().distanceToAsDouble(coord.toGeoPoint())
    }
}
